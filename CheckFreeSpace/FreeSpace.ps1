<#
Check Free Disk Space
Updated by 
Version 1.0
#>

#Variable 
$List = Get-Content ServerList.txt

#Get Disk Information from ServerList.txt
$StatsDate = Get-Date -Format F

Write-Output ""
Write-Output "Statictics captured at: $StatsDate"
Write-Output ""
Write-Output ""

foreach ( $args in $List) {
Write-Output $args
Get-WmiObject win32_logicaldisk -ComputerName $args -Filter "Drivetype=3"  | 
ft @{Label="Drive";Expression={$_.DeviceID}},@{Label="Label";Expression={$_.VolumeName}},
@{Label="TotalSize(GB)";Expression={$_.Size / 1gb -as [int] }},
@{Label="FreeSpace(GB)";Expression={$_.FreeSpace / 1gb -as [int] }},
@{Label="PercentageFree";Expression={(($_.FreeSpace/$_.Size)*100) -as [int]}} -AutoSize
} 
